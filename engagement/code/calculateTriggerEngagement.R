####################################################################################################################
#
#
# aktana- engagement estimates estimates Aktana Learning Engines.
#
# description: estimate the 
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################

library(data.table)

calculateTriggerEngagement <- function(chl, useForProbability)
{
    flog.info("calculateTriggerEngagement for channel %s ...", chl)

    events <- interactions[repActionTypeId==chl,list(accountId,repId,date,reaction)]    # subset the interactions for the right channel and today date
    events <- unique(events,by=c("accountId","repId","date"))                           # no need to have multiple records with the same account-rep-date combo

    suggestions[,date:=as.Date(date)]
    suggestions <- suggestions[repActionTypeId==chl & date<=today & date>=sugStartDate,list(repId,accountId,Suggestion_External_Id_vod__c,reaction,date)] # subset the suggestions
    setnames(suggestions,c("Suggestion_External_Id_vod__c"),c("suggestion"))            # rename fields

    suggestions$suggestion <- gsub("AKTSUG~","",suggestions$suggestion)                 # remove the AKTSUG~ prefix from the suggestion

    trig <- grep("TRIG",suggestions$suggestion)                                         # identify the trigger driven suggestions
    sugs <- suggestions[trig]                                                           # include them in the subsequent analysis

    sugs[!(reaction %in% includeReactions),reaction:="Ignored"]                         # if reaction to suggestion is not in parameter includeReactions then set the reaction to IGNORE                  
    sugs$recordId <- 1:nrow(sugs)                                                       # for subsequent processing number the records in the new suggestion table                                        
                                                                                                                                                                                                          
    ign <- sugs[reaction=="Ignored"]                                                    # subset the ignored suggestions                                                                                  
    if(nrow(ign)>0)                                                                     # if there are any ignored suggestions than find the time from suggestion to next action                          
    {                                                                                                                                                                                                     
        ign <- rbind(ign,events,fill=T)                                                 # append the ignored suggestions to the intereactions table (now called events)                                   
        setkey(ign,repId,accountId,date)                                                # sort ign table as setup to find time between suggestion and next action                                         
        ign[,dur:=c(NA,diff(date)),by=c("repId","accountId")]                           # add the dur field to the table that contains that difference in time                                            
        ign[,dur:=shift(dur,type="lead")]                                               # move the dur field values to the rows that contain the suggestion associated with the duration                  
        ign <- ign[reaction=="Ignored" & shift(reaction,type="lead")=="Touchpoint"]     # only include the appropriate types of differences                                                               
        sugs <- merge(sugs,ign[,c("recordId","dur"),with=F],by="recordId",all.x=T)      # merge the processed ignored table to the full suggestions table                                                 
        sugs <- sugs[is.na(dur),dur:=engageWindow+1]                                    # if there is no subsequent intereaction after a suggestionthan set the duration to one more than the engageWindow
    }
    sugs[reaction!="Ignored",dur:=0]                                                    # if suggestions are not ignored then set the duration to 0

    sugs$ctr <- 1                                                                       # setup to count the number of times that suggestions are engaged or no
    sugs$include <- F                                                                   # finish that setup                                                    
    sugs[dur<=engageWindow,include:=T]                                                  # this is where the suggestions <= engageWindow are included           
    sugs[,c("yes","total"):=list(sum(include),sum(ctr)),by=c("repId","accountId")]      # count the number of suggestions offered and accepted                 
    sugs[,probability:=yes/total,by=c("repId","accountId")]                             # that ratio is the probability estimate                               

    temp.A <- sugs[,list(repId,accountId,probability)]                                  # prepare to save the simple estimates of accepted trigger suggestions
    temp.A$repActionTypeId <- chl
    temp.A$runDate <- today+1
    #temp.A$suggestionType <- "Trigger-A"
    temp.A$date <- today+1

    temp.A$suggestionType <- "Trigger"

    total <- rbind(sugs[, list(accountId,repId,date,reaction)], events, fill=T)         # setup for estimating the likelihood of action after a suggestion and within the engageWindow                                                                   # setup for estimating the likelihood of action 
      
    setkey(total,repId,accountId,date,reaction)
    total[,c("nextValue","nextDate"):=list(shift(reaction,type="lead"),shift(date,type="lead")),by=c("repId","accountId")] # identify the next record in the merged event-suggestion table

    temp.T<-total[reaction %in% c("Accepted","Ignored") & nextValue=="Touchpoint"]     # pick out the accpentd and ignored
    
    temp.T[,difference:=nextDate-date]                                                 # calculate the time difference between suggestion and action
    temp.T$ctr <- 0
    temp.T[difference<=engageWindow,ctr:=1]                                            # count those records where the time difference is within the engageWindow
    temp.T[,numerator:=sum(ctr),by=c("accountId","repId")]                             # now count them
    temp.T$ctr <- 1
    temp.T[,denominator:=sum(ctr),by=c("accountId","repId")]                           # could all the suggestions
    temp.T[,probability:=numerator/denominator]                                        # the ratio is the probability estimate
    temp.T <- unique(temp.T,by=c("repId","accountId"))                                 # continue adding info for the save to the DB
    temp.T$repActionTypeId <- chl
    temp.T$runDate <- today+1
    temp.T$suggestionType <- "Trigger-T"
    temp.T$date <- today+1

    temp.T$suggestionType <- "Trigger"

    temp.T <- temp.T[, list(accountId,repId,date,suggestionType,runDate,repActionTypeId,probability)]
    
    dayEstimate <- interactions[repActionTypeId==chl & date<=today] 
    
    # estimates the likelihood of an interaction by day of week and week of month
    dayEstimate <- calculateDaysEstimate(dayEstimate, today, lookForward) 

    # prepare to spread the estimates over the lookForward interval
    temp.T <- addWeekdayWeekmonth(temp.T, today, lookForward)

    temp.T <- merge(temp.T, dayEstimate, by=c("repId","accountId","mw.wd"), all.x=T)

    temp.T[is.na(ratio), ratio:=0]

    temp.T[, probability:=ratio*probability]                                                 # finialize the separate estimates of prob touch, prob accept, and the combination     

    temp.T <- temp.T[, list(accountId,repId,date,suggestionType,runDate,repActionTypeId,probability)]

    if (useForProbability == "A") {
      triggerEngage <- temp.A
    }
    else if (useForProbability == "T") {
      triggerEngage <- temp.T
    } 
    else {
      triggerEngage <- rbind(temp.A, temp.T)
    }

    triggerEngage[, c("learningRunUID","learningBuildUID","repUID","accountUID"):=list(RUN_UID,BUILD_UID,repId,accountId)]

    return(triggerEngage[,list(learningRunUID,learningBuildUID,repActionTypeId,repUID,accountUID,suggestionType,date,probability)])
}
