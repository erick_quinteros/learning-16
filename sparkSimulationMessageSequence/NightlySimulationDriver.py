import sys
import os
import subprocess

# Update sys path to find the modules from Common and DataAccessLayer
script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
learning_dir = os.path.dirname(script_dir)
sys.path.append(learning_dir)

from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from common.DataAccessLayer.DataAccessLayer import DSEAccessor

from sparkSimulationMessageSequence.LearningPropertiesReader import LearningPropertiesReader
from sparkSimulationMessageSequence.LearningPropertiesKey import LearningPropertiesKey

LEARNING_PROPERTIES_FILE_NAME = "learning.properties"
LEARNING_DB_SUFFIX = "_learning"
PARALLEL_PROCESS_COUNT = 3

learning_home_dir = None


def get_mso_build_uid_list():
    """
    This function returns the list of MSO Build UIDs to process
    :return:
    """
    mso_build_uids = []
    dse_accessor = DSEAccessor()
    external_ids = dse_accessor.get_active_undeleted_message_algorithm_externalIds()
    for external_id in external_ids:
        print("Processing external Id: " + external_id)
        learning_prop_file_path = os.path.join(learning_home_dir, 'builds', 'nightly', external_id, LEARNING_PROPERTIES_FILE_NAME)
        learning_prop_config = read_learning_properties(learning_prop_file_path)
        mso_build_uid = learning_prop_config.get_property(LearningPropertiesKey.BUILD_UID)
        mso_build_uids.append(mso_build_uid)

    return mso_build_uids

def read_learning_properties(learning_prop_file_path):
    """
    This function reads learning properties from the specified build directory.
    :return:
    """
    assert os.path.isfile(learning_prop_file_path), "Learning Properties file does not exists " + learning_prop_file_path

    learning_prop_config = LearningPropertiesReader()
    learning_prop_config.read_learning_properties(learning_prop_file_path)

    return learning_prop_config

def submit_simulation_job(mso_build_uid):
    """
    This function will submit a spark simulation job with specified MSO Build UID.
    :param mso_build_uid:
    :return:
    """
    global learning_home_dir
    run_python_script_path = os.path.join(learning_home_dir, "common/sparkUtils/run-nightly-simulation.sh")
    simulation_driver_path = os.path.join(learning_home_dir, "sparkSimulationMessageSequence/SimulationMessageSequenceDriver.py")

    cmd_arguments = [run_python_script_path, simulation_driver_path]
    cmd_arguments.extend(sys.argv[1:])
    cmd_arguments.append(mso_build_uid)
    print("Argument List: ", cmd_arguments)
    sub_process = subprocess.Popen(cmd_arguments)
    return sub_process

def main():
    """
    This is main function for running simulation for message sequence in parallel
    :return: 
    """
    global learning_home_dir

    # Validate the input argument to the script
    if len(sys.argv) != 10:
        print("Invalid arguments to the simulation script")

    # Retrieve the arguments
    db_host = sys.argv[1]
    db_user = sys.argv[2]
    db_password = sys.argv[3]
    dse_db_name = sys.argv[4]
    cs_db_name = sys.argv[5]
    stage_db_name = sys.argv[6]
    learning_db_name = sys.argv[7]
    db_port = sys.argv[8]
    learning_home_dir = sys.argv[9]

    # Create Learning database name
    learning_db_name = dse_db_name + LEARNING_DB_SUFFIX

    # Get the singleton instance of the database config and set the properties
    database_config = DatabaseConfig.instance()
    database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name)

    mso_build_uids = get_mso_build_uid_list()

    print("MSO Build UIDs: ", mso_build_uids)

    running_processes = []

    for mso_build_uid in mso_build_uids:
        running_processes.append(submit_simulation_job(mso_build_uid))

    exit_codes = [running_process.wait() for running_process in running_processes]

    print("Simulation Run Completed with exit codes ", exit_codes)


if __name__ == "__main__":
    main()

