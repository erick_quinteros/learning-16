##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: read the DB and save in list in .RData file
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
#
##########################################################

readV3db.messages <- function(con) {
  flog.info("read messages")
  messages <- data.table(dbGetQuery(con,"Select m.messageId, m.lastPhysicalMessageUID, m.messageTopicId, m.messageName, m.messageDescription, m.messageChannelId, p.productName FROM Message as m JOIN Product p using(productId);"))
  return(messages)
}

readV3db.messageSetMessage <- function(con) {
  flog.info("read messageSetMessage")
  messageSetMessage <- data.table(dbGetQuery(con,"Select messageId, messageSetId FROM MessageSetMessage;"))
  return(messageSetMessage)
}

readV3db.messageSet <- function(con) {
  flog.info("read messageSet")
  messageSet <- data.table(dbGetQuery(con,"Select messageSetId, externalId FROM MessageSet;"))
  return(messageSet)
}

readV3db.getFilterAcctsList <- function(con) {
  flog.info("get filtered accts list using Reps and RepAccountAssignment table")
  reps <- data.table(dbGetQuery(con,"SELECT r.repId, r.repName, rtr.repTeamName FROM Rep as r JOIN RepTeamRep using (repId) JOIN RepTeam rtr using (repTeamId) WHERE isActivated=1;"))
  repAssignments <- data.table(dbGetQuery(con,"SELECT repId, accountId FROM RepAccountAssignment;"))
  accts <- unique(repAssignments[repId %in% reps$repId]$accountId)
  return(accts)
}

readV3db.accounts <- function(con, accts) {
  flog.info("read accounts")
  accounts <- data.table(dbGetQuery(con,"SELECT * FROM Account WHERE isDeleted=0;"))
  accounts <- accounts[accountId %in% accts]
  return(accounts)
}

readV3db.products <- function(con, productUID) {
  flog.info("read products")
  products <- data.table(dbGetQuery(con,"SELECT productName, externalId, productId FROM Product WHERE isActive=1 and isDeleted=0;"))
  if (productUID!="All") {
    products <- products[products$externalId==productUID]
  }
  return(products)
}

readV3db.accountProduct <- function(con, accts, products, prods=NULL) {
  if (dim(products)[1]==1) { # productUID != 'All'
    flog.info("read accountProduct: %s",products$productName)
    accountProductSQL <- sprintf("SELECT * FROM AccountProduct where productId=%s;", products$productId)
  } else {
    flog.info("read accountProduct: All")

    acctProd <- data.table(dbGetQuery(con, "select * from AccountProduct limit 1"))
    acctProd.names <- names(acctProd)

    # get only column names in AccountProduct table
    if (length(prods) > 0) {
      prods <- prods[prods %in% acctProd.names]
    }

    if(prods == 0 || length(prods) == 0 ) {
      accountProductSQL <- "SELECT accountId, productId FROM AccountProduct"
    } else if (length(prods) >= 100){
      accountProductSQL <- sprintf("SELECT accountId, productId, %s FROM AccountProduct WHERE rand() <= .05", paste(unlist(prods), collapse=","))
    } else{
      accountProductSQL <- sprintf("SELECT accountId, productId, %s FROM AccountProduct", paste(unlist(prods), collapse=","))
    }
     
    flog.info("accountProductSQL=%s", accountProductSQL)
  }
  accountProduct <- data.table(dbGetQuery(con, accountProductSQL))
  # filter by accts
  accountProduct <- accountProduct[accountId %in% accts]
  # add productName
  accountProduct <- merge(accountProduct, products[,c("productId","productName")], by="productId")
  accountProduct$productId <- NULL
  return(accountProduct)
}

readV3db.interactions <- function(con, accts, products) {
  if (dim(products)[1]==1) { # productUID != 'All'
    flog.info("read interactions: %s",products$productName)
    interactionsSQL <- sprintf("SELECT  v.interactionId, v.externalId, v.repId, v.facilityId, v.wasCreatedFromSuggestion, ia.accountId, IFNULL(ipm.messageId, vp.messageId) as messageId, IFNULL(ipm.physicalMessageUID, vp.physicalMessageUID) as physicalMessageUID, vp.productId, vp.productInteractionTypeId, v.duration, v.startDateLocal FROM Interaction as v JOIN InteractionProduct vp ON v.isCompleted=1 AND v.isDeleted=0 AND vp.productId=%s AND v.interactionId=vp.interactionId JOIN InteractionAccount ia ON v.interactionId=ia.interactionId LEFT JOIN InteractionProductMessage ipm ON vp.`interactionId` = ipm.`interactionId` AND vp.`productId` = ipm.`productId`;", products$productId)
  } else {
    flog.info("read interactions: All")
    interactionsSQL <- "SELECT  v.interactionId, v.externalId, v.repId, v.facilityId, v.wasCreatedFromSuggestion, ia.accountId, IFNULL(ipm.messageId, vp.messageId) as messageId, IFNULL(ipm.physicalMessageUID, vp.physicalMessageUID) as physicalMessageUID, vp.productId, vp.productInteractionTypeId, v.duration, v.startDateLocal FROM Interaction as v JOIN InteractionProduct vp ON v.isCompleted=1 AND v.isDeleted=0 AND v.interactionId=vp.interactionId JOIN InteractionAccount ia ON v.interactionId=ia.interactionId LEFT JOIN InteractionProductMessage ipm ON vp.`interactionId` = ipm.`interactionId` AND vp.`productId` = ipm.`productId`;"
  }
  interactions <- data.table(dbGetQuery(con,interactionsSQL))
  # appending productInteractionTypeName
  productInteractionType <- data.table(dbGetQuery(con, "SELECT productInteractionTypeId, productInteractionTypeName FROM ProductInteractionType;"))
  interactions <- merge(interactions, productInteractionType, by="productInteractionTypeId")
  interactions$productInteractionTypeId <- NULL
  # filter by accts
  interactions <- interactions[accountId %in% accts]
  # change date data type
  interactions$date <- as.Date(interactions$startDateLocal)
  interactions$startDateLocal <- NULL
  # add productName
  interactions <- merge(interactions, products[,c("productId","productName")], by="productId")
  interactions$productId <- NULL
  return(interactions)
}

readV3db.events <- function(con, accts, products) {
  if (dim(products)[1]==1) { # productUID != 'All'
    flog.info("read events: %s",products$productName)
    eventsSQL <- sprintf("SELECT  e.repId, e.accountId, e.eventDate, et.eventTypeName, e.eventLabel, e.eventDateTimeUTC, e.messageId, e.physicalMessageUID, e.productId FROM .Event as e JOIN .EventType et ON e.productId=%s AND e.eventTypeId=et.eventTypeId;", products$productId)
  } else {
    flog.info("read events: All")
    eventsSQL <- "SELECT  e.repId, e.accountId, e.eventDate, et.eventTypeName, e.eventLabel, e.eventDateTimeUTC, e.messageId, e.physicalMessageUID, e.productId FROM .Event as e JOIN .EventType et ON e.eventTypeId=et.eventTypeId;"
  }
  events <- data.table(dbGetQuery(con,eventsSQL))
  # filter by accts
  events <- events[accountId %in% accts]
  # add prodcutName
  events <- merge(events, products[,c("productId","productName")], by="productId")
  events$productId <- NULL
  return(events)
}


##########################################################################################################
#                                               Main Function
###########################################################################################################

readV3dbFilter <- function(con, productUID, readDataList=NULL, prods=NULL)
{
  if (is.null(readDataList)) {
    readDataList <- c("accounts","interactions","messages","messageSetMessage","messageSet","accountProduct","events","products")
    flog.info("read V3db data: all (%s)", paste(readDataList,collapse=","))
  } else {
    flog.info("read V3db data: %s", paste(readDataList,collapse=","))
  }
  # initialize loading
  loadedData <- list()
  dbGetQuery(con,"SET NAMES utf8;")
  
  # table loading not affected by the input productUID and accts filtering
  if ("messages" %in% readDataList) {
    loadedData[["messages"]] <- readV3db.messages(con)
  }

  if ("messageSetMessage" %in% readDataList) {
    loadedData[["messageSetMessage"]] <- readV3db.messageSetMessage(con)
  }

  if ("messageSet" %in% readDataList) {
    loadedData[["messageSet"]] <- readV3db.messageSet(con)
  }
  
  # table loading needs accts filter
  if (any(readDataList %in% c("accounts","interactions","accountProduct","events"))) {
    accts <- readV3db.getFilterAcctsList(con)
  }

  if ("accounts" %in% readDataList) {
    loadedData[["accounts"]] <- readV3db.accounts(con,accts)
  }

  # table loading affected by the input productUID
  if (any(readDataList %in% c("interactions","accountProduct","events"))) {
    products <- readV3db.products(con,productUID)
  }
  
  if ("accountProduct" %in% readDataList) {
    loadedData[["accountProduct"]] <- readV3db.accountProduct(con,accts,products,prods)
  }

  if ("interactions" %in% readDataList) {
    loadedData[["interactions"]] <- readV3db.interactions(con,accts,products)
  }

  if ("events" %in% readDataList) {
    loadedData[["events"]] <- readV3db.events(con,accts,products)
  }

  if ("products" %in% readDataList) {
    loadedData[["products"]] <- products[,c("productName","externalId")]
  }
  
  flog.info("return from V3readFilter")
  return(loadedData)
}

