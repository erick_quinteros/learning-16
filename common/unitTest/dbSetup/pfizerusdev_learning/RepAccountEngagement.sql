CREATE TABLE `RepAccountEngagement` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repActionTypeId` int(11) DEFAULT NULL,
  `repUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accountUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suggestionType` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `probability` double DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `RepAccountEngagement_account_index` (`learningRunUID`,`accountUID`),
  KEY `RepAccountEngagement_rep_index` (`learningRunUID`,`repUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci