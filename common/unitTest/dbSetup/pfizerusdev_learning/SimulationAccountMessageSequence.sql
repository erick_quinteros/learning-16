CREATE TABLE `SimulationAccountMessageSequence` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `accountUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `messageSeqUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `probability` double DEFAULT NULL,
  `isFinalized` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `accountName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  KEY `learningBuildUID_idx` (`learningBuildUID`),
  KEY `messageSeqID_idx` (`messageSeqUID`),
  KEY `SimulationAccountMessageSequence_fk_2` (`learningRunUID`)
  /* CONSTRAINT `SimulationAccountMessageSequence_fk_1` FOREIGN KEY (`learningBuildUID`) REFERENCES `LearningBuild` (`learningBuildUID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SimulationAccountMessageSequence_fk_2` FOREIGN KEY (`learningRunUID`) REFERENCES `LearningRun` (`learningRunUID`) ON DELETE CASCADE ON UPDATE CASCADE */
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
