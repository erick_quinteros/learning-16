CREATE TABLE `LearningFile` (
  `learningFileUID` varchar(80) NOT NULL,
  `learningRunUID` varchar(80) DEFAULT NULL,
  `learningBuildUID` varchar(80) NOT NULL,
  `fileName` varchar(80) NOT NULL,
  `fileType` varchar(20) NOT NULL,
  `fileSize` int(10) NOT NULL,
  `data` longblob,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`learningFileUID`),
  KEY `learningFile_fk_1_idx` (`learningRunUID`),
  KEY `learningFile_fk_2_idx` (`learningBuildUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8